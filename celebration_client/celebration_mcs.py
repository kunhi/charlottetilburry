
import json
import time
import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
import vlc
import time
import random

#~ from ola.ClientWrapper import ClientWrapper
from dmx_up import TheDmx
from twisted.internet import reactor
from twisted.internet.protocol import ReconnectingClientFactory
from twisted.python import log
from universe import create_universe_if_not

from autobahn.twisted.websocket import WebSocketClientFactory, WebSocketClientProtocol, connectWS

PING_INTERVAL = 2 #time in seconds between ping
PING_TIMEOUT = 5 #ping time out in seconds
RECONNECT_MAXDELAY = 10 #max time in seconds between reconection attempts

# the vlc instance
the_vlc_instance = vlc.Instance()
the_player = the_vlc_instance.media_player_new()

# dmx called 
the_dmx = None

# sond finished function
def SongFinished(event):
	print("song finished")
	global the_dmx
	the_dmx.stop()
	time.sleep(1)
	msg = json.dumps({"celebration": "stop"})
	send(msg)

def the_vlc_player():
	global the_vlc_instance, the_player
	the_player = the_vlc_instance.media_player_new()
	media = the_vlc_instance.media_new_path('/home/pi/Documents/clockTower/big_ben_chimes2bells.mp3') #Your audio file here
	the_player.set_media(media)
	events = the_player.event_manager()
	events.event_attach(vlc.EventType.MediaPlayerEndReached, SongFinished)
	the_player.play()

class ClientProtocol(WebSocketClientProtocol):

	def onConnect(self, response):
		global websocket
		websocket = self
		print("Server connected: {0}".format(response.peer))
		#beep(1)


	def onOpen(self):
		print("WebSocket connection open.")
		#run(self)
		# check_thread = threading.Thread(target=initialize_state, args=[self])
		# check_thread.start()

	def onMessage(self, payload, isBinary):
		print("payload "+str(payload))
		global the_dmx, the_player
		my_json = payload.decode('utf8').replace("'", '"')
		data = json.loads(my_json)
		
		print(data)
		if data.get("message") == "start":
			msg = json.dumps({"celebration": "start"})
			send(msg)
			the_dmx = TheDmx()
			the_dmx.start()
			the_vlc_player()
		elif data.get("message") == "stop":
			if the_dmx:
				the_dmx.stop()
			the_player.stop()
			msg = json.dumps({"celebration": "stop"})
			send(msg)	

	def onClose(self, wasClean, code, reason):
		print("close")
		print("code "+str(code))
		print("reason "+str(reason))
		# callback_disconnect()




class ClientFactory(ReconnectingClientFactory, WebSocketClientFactory):

	protocol = ClientProtocol

	maxDelay = RECONNECT_MAXDELAY
#    maxRetries = 5

	def startedConnecting(self, connector):
		print('Started to connect.')

	def clientConnectionLost(self, connector, reason):
		print('Lost connection. Reason: {}'.format(reason))
		ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

	def clientConnectionFailed(self, connector, reason):
		print('Connection failed. Reason: {}'.format(reason))
		ReconnectingClientFactory.clientConnectionFailed(self, connector, reason)




def send(msg):
	global websocket
	message_bytes = str.encode(msg)
	websocket.sendMessage(message_bytes, False)


def init():
	"""
	initializes the webscoket connection, providing calbacks for start and reset message.
	:param server_adress: server adress
	:param start_function: launched when start is received
	:param reset_function: launched when reset is received
	:param disconnect_function: launched when disconnected from server
	:return: nichts
	"""

	global factory

	# rep = directory
	
	server = u"ws://192.168.0.102:8000/ws/promoter/celebration/fixed/"

	#log.startLogging(sys.stdout)
	factory = ClientFactory(server)
	factory.protocol = ClientProtocol
	factory.setProtocolOptions(autoPingInterval=PING_INTERVAL,
							   autoPingTimeout=PING_TIMEOUT,
							   failByDrop=False)

	connectWS(factory)

	reactor.run()	

create_universe_if_not()

init() 
