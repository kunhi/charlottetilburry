# Generated by Django 3.0.2 on 2020-01-19 15:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('promoter', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='towerclock',
            name='status',
        ),
        migrations.AddField(
            model_name='towerclock',
            name='cele_status',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='towerclock',
            name='phone_id',
            field=models.CharField(blank=True, default=None, max_length=200, null=True),
        ),
    ]
