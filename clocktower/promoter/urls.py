# chat/urls.py
from django.urls import path

from . import views

urlpatterns = [
    path('control/', views.index, name='index'),
    path('telephone/<str:room_name>/', views.room, name='room'),
]