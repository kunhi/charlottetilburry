# chat/consumers.py
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from .models import Towerclock 
import json
import time


class Control(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        the_current_record = Towerclock.objects.get(id=1)
        the_current_record.phone_id = message if message != "phone_0" else None
        the_current_record.save()
        time.sleep(0.3)
        # Send message to room group
        # if message != None:
        for each in ["phone_1", "phone_2", "phone_3"]:
            async_to_sync(self.channel_layer.group_send)(
                "chat_"+str(each),
                {
                    'type': 'chat_message',
                    'message': message
                }
            )

        self.send(text_data=json.dumps({
            'message': "saved"
        }))    
    # Receive message from room group
    def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))



class Telephone(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name
        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json["celebration"]
        if(message == "start"):
            the_current_record = Towerclock.objects.get(id=1)
            print(the_current_record.cele_status)
            if the_current_record.cele_status == False:
                    
                    for each in ["phone_1", "phone_2", "phone_3"]:
                        async_to_sync(self.channel_layer.group_send)(
                            "chat_"+str(each),
                            {
                                'type': 'chat_message',
                                'message': "None"
                            }
                        )

                    async_to_sync(self.channel_layer.group_send)(
                        'chat_fixed',
                        {
                            'type': 'chat_message',
                            'message': "start"
                        }
                    )

                    async_to_sync(self.channel_layer.group_send)(
                        'chat_test',
                        {
                            'type': 'chat_message',
                            'message': "cele_start"
                        }
                    )
                    the_current_record.cele_status = True
                    the_current_record.save()
                    time.sleep(0.5)
        elif(message == "stop"):
            # async_to_sync(self.channel_layer.group_send)(
            #             'chat_fixed',
            #             {
            #                 'type': 'chat_message',
            #                 'message': "stop"
            #             }
            #         )

            async_to_sync(self.channel_layer.group_send)(
                'chat_test',
                {
                    'type': 'chat_message',
                    'message': "cele_stop"
                }
            )            
                    # the_current_record.cele_status = True
                    # the_current_record.save()
        print(text_data_json)
        print(text_data)

    # Receive message from room group
    def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))


class Celebration(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name
        print(self.room_group_name)
        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['celebration']

        if(message == "stop"):
            the_current_record = Towerclock.objects.get(id=1)
            the_current_record.phone_id = None
            the_current_record.cele_status = False
            the_current_record.save()
            time.sleep(0.5)
            for each in ["phone_1", "phone_2", "phone_3"]:
                async_to_sync(self.channel_layer.group_send)(
                    "chat_"+str(each),
                    {
                        'type': 'chat_message',
                        'message': "none"
                    }
                )

            async_to_sync(self.channel_layer.group_send)(
                'chat_test',
                {
                    'type': 'chat_message',
                    'message': "cele_stop"
                }
            )
        elif(message == "start"):
            async_to_sync(self.channel_layer.group_send)(
                'chat_test',
                {
                    'type': 'chat_message',
                    'message': "cele_start"
                }
            )

    # Receive message from room group
    def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))        