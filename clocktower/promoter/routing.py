# chat/routing.py
from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
	re_path(r'ws/promoter/control/(?P<room_name>\w+)/$', consumers.Control),
    re_path(r'ws/promoter/telephone/(?P<room_name>\w+)/$', consumers.Telephone),
    re_path(r'ws/promoter/celebration/(?P<room_name>\w+)/$', consumers.Celebration),
]