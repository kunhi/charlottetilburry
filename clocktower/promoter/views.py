from django.shortcuts import render
from .models import Towerclock

def index(request):
	the_current_record = Towerclock.objects.get(id=1)
	return render(request, 'promoter/index.html', {"the_record":the_current_record})

def room(request, room_name):
	return render(request, 'promoter/room.html', {
		'room_name': room_name
	})