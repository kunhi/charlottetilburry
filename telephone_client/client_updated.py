import json
import time
import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
import vlc
import time
import random

from twisted.internet import reactor
from twisted.internet.protocol import ReconnectingClientFactory
from twisted.python import log

from autobahn.twisted.websocket import WebSocketClientFactory, WebSocketClientProtocol, connectWS

PING_INTERVAL = 2 #time in seconds between ping
PING_TIMEOUT = 5 #ping time out in seconds
RECONNECT_MAXDELAY = 10 #max time in seconds between reconection attempts


play_winning_song = False

receiver_took = False

the_random_int = None

the_vlc_instance = None

the_player = None

the_mp3_hut = {"1":"/home/pi/Documents/clockTower/mp3/VO04.wav", "2":"/home/pi/Documents/clockTower/mp3/VO03.wav"}


class ClientProtocol(WebSocketClientProtocol):

	def onConnect(self, response):
		global websocket
		websocket = self
		print("Server connected: {0}".format(response.peer))
		#beep(1)


	def onOpen(self):
		print("WebSocket connection open.")
		#run(self)
		# check_thread = threading.Thread(target=initialize_state, args=[self])
		# check_thread.start()

	def onMessage(self, payload, isBinary):
		global play_winning_song
		print("payload "+str(payload))
		my_json = payload.decode('utf8').replace("'", '"')
		data = json.loads(my_json)
		
		if(data["message"] == "win"):
			play_winning_song = True
		else:
			play_winning_song = False	
		
		print(data)



	def onClose(self, wasClean, code, reason):
		print("close")
		print("code "+str(code))
		print("reason "+str(reason))
		# callback_disconnect()




class ClientFactory(ReconnectingClientFactory, WebSocketClientFactory):

	protocol = ClientProtocol

	maxDelay = RECONNECT_MAXDELAY
#    maxRetries = 5

	def startedConnecting(self, connector):
		print('Started to connect.')

	def clientConnectionLost(self, connector, reason):
		print('Lost connection. Reason: {}'.format(reason))
		ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

	def clientConnectionFailed(self, connector, reason):
		print('Connection failed. Reason: {}'.format(reason))
		ReconnectingClientFactory.clientConnectionFailed(self, connector, reason)




def send(msg):
	global websocket
	message_bytes = str.encode(msg)
	websocket.sendMessage(message_bytes, False)


def init():
	"""
	initializes the webscoket connection, providing calbacks for start and reset message.
	:param server_adress: server adress
	:param start_function: launched when start is received
	:param reset_function: launched when reset is received
	:param disconnect_function: launched when disconnected from server
	:return: nichts
	"""

	global factory

	# rep = directory
	
	server = u"ws://192.168.0.102:8000/ws/promoter/telephone/phone_1/"

	#log.startLogging(sys.stdout)
	factory = ClientFactory(server)
	factory.protocol = ClientProtocol
	factory.setProtocolOptions(autoPingInterval=PING_INTERVAL,
							   autoPingTimeout=PING_TIMEOUT,
							   failByDrop=False)

	connectWS(factory)

	reactor.run()

def the_winner_song_finished(event):
	global the_winner_flag, the_button_pressed, play_winning_song
	the_winner_flag = False
	print("the winner song end")
	if not play_winning_song:
		the_vlc_player_for_winner()

def the_vlc_player_for_winner():
	global 	the_vlc_instance, the_player
	time.sleep(0.3)
	the_vlc_instance = vlc.Instance()
	the_player = the_vlc_instance.media_player_new()
	media = the_vlc_instance.media_new_path(the_mp3_hut["2"]) #Your audio file here
	the_player.set_media(media)
	events = the_player.event_manager()
	events.event_attach(vlc.EventType.MediaPlayerEndReached, the_winner_song_finished)
	the_player.play()	

def the_vlc_player_normal(the_mp3_file):
	global  the_vlc_instance, the_player
	time.sleep(0.3)
	the_vlc_instance = vlc.Instance()
	the_player = the_vlc_instance.media_player_new()
	media = the_vlc_instance.media_new_path(the_mp3_file) #Your audio file here
	the_player.set_media(media)
	the_player.play()   

def phone_receiver(channel):
	print("hello here we go!")
	global receiver_took, the_random_int, play_winning_song, the_vlc_instance
	if(GPIO.input(14) == 1):
		
		if receiver_took == False:
			receiver_took = True
			if not play_winning_song:
				time.sleep(0.2)
				the_vlc_player_normal(the_mp3_hut["1"])
			else:
				time.sleep(0.2)
				the_vlc_player_normal(the_mp3_hut["2"])
				msg = json.dumps({"phone": "phone_1", "celebration": "start"})
				send(msg)  
	elif(GPIO.input(14) == 0):
		global the_player, play_winning_song, receiver_took		
		time.sleep(0.3)
		receiver_took = False
		the_player.stop()
		receiver_took = False
		#~ if play_winning_song:
			#~ play_winning_song = False
			#~ msg = json.dumps({"phone": "phone_1", "celebration": "stop"})
			#~ send(msg)

GPIO.cleanup() # Clean up           
GPIO.setwarnings(False) # Ignore warning for now
GPIO.setmode(GPIO.BCM) # Use physical pin numbering
GPIO.setup(14, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)    
GPIO.add_event_detect(14,GPIO.BOTH,callback=phone_receiver, bouncetime=200)
	


init() 
